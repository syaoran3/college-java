package cardgame;

import java.util.ArrayList;

public class Deck {

	private int n = 1;
	private	String[] suit = new Card().getSuit();
	private int suitN = 0;
	private ArrayList<String> cardNames = new ArrayList<String>();
	private ArrayList<String> cardValues = new ArrayList<String>();
	/**
	 * @return the cardNames
	 */
	public ArrayList<String> getCardNames() { //gets the card name arraylist
		return cardNames;
	}

	/**
	 * @return the cardValues
	 */
	public ArrayList<String> getCardValues() { //gets the card value arraylist
		return cardValues;
	}

	

	public Deck(){
		for(int i = 0; i < 52; i++){
			switch(n){
			case 13: 
				cardNames.add(i, suit[suitN] + "King"); //add the card name to the deck
				cardValues.add(i, Integer.toString(10)); //add the card value to the deck
				break;
			case 12:
				cardNames.add(i,suit[suitN] + "Queen");//add the card name to the deck
				cardValues.add(i,Integer.toString(10));//add the card value to the deck
				break;
			case 11:
				cardNames.add(i, suit[suitN] + "Jack");//add the card name to the deck
				cardValues.add(i, Integer.toString(10));//add the card value to the deck
				break;
			case 1:
				cardNames.add(i, suit[suitN] + "Ace");//add the card name to the deck
				cardValues.add(i, Integer.toString(n));//add the card value to the deck
				break;
			default:
				cardNames.add(i, suit[suitN] + Integer.toString(n));
				cardValues.add(i, Integer.toString(n));//add the card value to the deck
				break;
			}
			if(n >= 13) {n=0;suitN += 1;}
			n++;
		}
	}

	public void removeCard(Deck deck, int index) {
		deck.cardNames.remove(index);//removes the card from the deck
		deck.cardValues.remove(index+1);//removes the card from the deck
	}


}
