package cardgame;

public class Card {

	private String[] suit = {"♣","♦","♥","♠"}; //Stores the suit unicode

	/**
	 * @return the suit
	 */
	public String[] getSuit() {
		return suit;
	}
}
