package cardgame;
import java.util.Random;
import java.util.Scanner;
public class Main {
	public static boolean stick = false;
	public static boolean bust = false;
	public static boolean quit = false;
	public static int starter = 0;
	public static void main(String[] args) {
		Player player1 = new Player(); //New Instance of Player
		Player player2 = new Player(); //New Instance of Player
		Deck deck = new Deck(); //New instance of Deck
		Scanner scan = new Scanner(System.in);
		while (stick == false && bust == false) {
			while (player2.getTotal() < 17) { // Loop for CPU
				player2.setRandom(getRandomCard(deck)); //store the random card picked from the deck
				deck.removeCard(deck, player2.getRandom()); //remove the card from the deck so that it can't be drawn again
				player2.addToTotal(Integer.parseInt(deck.getCardValues().get(player2.getRandom()))); //add the value of the card to the players total
			}
			//PLAYER 1
			if(starter <= 0){
				while (starter <= 0) {
					player1.setRandom(getRandomCard(deck)); //get a random number from the deck
					String rand = deck.getCardNames().get(player1.getRandom()); //find out what card the random number is linked to
					deck.removeCard(deck, player1.getRandom()); //remove the card from the deck that the player has pulled
					player1.addToTotal(Integer.parseInt(deck.getCardValues().get(player1.getRandom()))); //add the value of the card to the player's total
					System.out.println("You drew " + rand); //print the message of the card
					starter++; //Increment the starter int
				}
			}else {
				player1.setRandom(getRandomCard(deck)); //get a random number from the deck
				String rand = deck.getCardNames().get(player1.getRandom()); //find out what card the random number is linked to
				deck.removeCard(deck, player1.getRandom()); //remove the card from the deck that the player has pulled
				player1.addToTotal(Integer.parseInt(deck.getCardValues().get(player1.getRandom()))); //add the value of the card to the player's total
				System.out.println("You drew " + rand + "\nCurrent Total:" + player1.getTotal()); //output the card you drew and the current total
				if (player1.getTotal() > 21) { //check if the players total is above 21, if true:
					bust = true; //set the bust value to true
					System.out.println("BUST"); //output "BUST"
				}else if(player2.getTotal() > 21){
					bust = true;
					System.out.println("CPU BUST");
				}else{
					System.out.println("Stick? true/false");
					String ans = scan.nextLine();
					if (ans.contains("t")){ //if the player enters 't' then true
						stick = true;
					}else{ //otherwise set to false
						stick = false;
					} 
				}
			}
		}
		scan.close();
		System.out.println("Your total is : " + player1.getTotal()); //output the text for your total
		System.out.println("CPU total is : " + player2.getTotal()); //output the text for the cpu total
		if (player1.getTotal() == player2.getTotal()){
			System.out.println("You draw");
		}else if(player1.getTotal() <= 21 && ( player2.getTotal() > 21 || Math.abs(player1.getTotal()-21) <= Math.abs(player2.getTotal()-21))){
			System.out.println("You win~");
		}else {
			System.out.println("You lost :P");
		}
	}

	public static int getRandomCard(Deck deck) {
		return new Random().nextInt(deck.getCardNames().size() - 1);//This generates a random number from 0 to the size of the array
	}
}