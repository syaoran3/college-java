package cardgame;

public class Player {

	private int total;
	private int random;
 
	public int getRandom() {
		return random;
	}

	public void setRandom(int random) { // store the number generated by another method as the random number
		this.random = random;
	}

	public Player() { //initializer
		total = 0;
	}

	public int getTotal() { //return the total
		return total;
	}

	public void setTotal(int total) {//set the total
		this.total = total;
	}
	
	public void addToTotal(int total) { //add int to the total
		this.total += total;
	}

}
